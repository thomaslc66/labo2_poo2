/*
 -----------------------------------------------------------------------------------
 Laboratoire : Laboratoire 02
 Fichier     : cstring.cpp
 Auteur(s)   : Thomas Lechaire et Michael Brouchoud
 Date        : 09.04.2017

 But         : Implementation d'une class String

 Compilateur : Cygwin 2.4.1-1
 gcc 5.3.0
 g++ 5.3.0
 make 4.1
 gbd 7.10.1-1

 Remarque(s) : -

 -----------------------------------------------------------------------------------
 */

#include "cstring.h"
#include <string.h>
#include <math.h>

using namespace std;

ostream& operator << (ostream& stream, const String& str) {

    for (int i = 0; i < str.length; i++) {
        stream << str.at(i);
    }

    return stream;
}

void String::iniString(const char* chain) {
    this->length = strlen(chain);
    this->str = new char[length];
    strcpy(this->str, chain);
}

char& String::operator[](const size_t index) {
    return this->at(index);
}

String& String::operator = (const String& str) {
    if(this != &str) {
        delete[] this->str;
        iniString(str.str);
    }
    return *this;
}

String String::operator + (const String& str) const {
    String tmp;
    tmp += *this;
    tmp += str;
    return tmp;
}

String& String::operator += (const String& str) {
    return this->append(str);
}

String& String::operator = (const char* chain) {
    return *this = String(chain);
}

String String::operator + (const char* chain) const {
    return *this + String(chain);
}

String& String::operator += (const char* chain) {
    return *this += String(chain);
}

String String::operator + (const char car) const {
    return *this + String(car);
}

String& String::operator += (const char car) {
    return *this += String(car);
}

bool String::operator == (const String& str) const {
    return (bool)strcmp(this->str, str.str);
}

bool String::operator != (const String& str) const {
    return !(*this == str);
}

String::String() : String("") { }

String::String(const char* chain) {
    iniString(chain);
}

String::String(const String& chain) : String(chain.str) {}

String::String(const char car) : String(new char(car)) {
    char tmp[2]; //Permet de creer une chaine de caractere contenant un '\0' a la fin
    tmp[0] = car;
    tmp[1] = '\0';
    iniString(tmp);
}

String::String(const int num) : String() {
    *this = convertIntToString(num);
}

String::String(const float flt) : String() {
    *this = convertFloatToString(flt);
}

String::String(const bool bln) : String() {
    if(bln) {
        *this = String("true");
    }
    else {
        *this = String("false");
    }
}

String::~String(){
    delete[] str;
}

const size_t String::getSize() const {
    return this->length;
}

char& String::at(const size_t index) const{
    if(index < this->length) {
        return this->str[index];
    }
    else {
        throw out_of_range((String("String.at(") + String((int)index) + String(") : index invalid for String : ") + this->str).print());
    }
}

const char* String::print() const {
    return this->str;
}

String String::substring(const size_t start, const size_t end) const {
    String sub;

    if(end < start) {
        throw invalid_argument((String("String.substring() : invalid argument for String : ") + this->str).print());
    }

    try {
        for(size_t i = start; i < end; i++) {
            sub += this->at(i);
        }
    }catch (out_of_range e) {
        throw out_of_range((String("String.substring() : ") + String(e.what())).print());
    }
    return sub;
}

void String::setString(const String& str) {
    *this = str;
}

void String::setString(const char* chain) {
    *this = chain;
}

bool String::equals(const String& str) const {
    *this == str;
}

bool String::notEquals(const String& str) const {
    *this != str;
}

bool String::equals(const char* chain) const {
    *this == chain;
}

bool String::notEquals(const char* chain) const {
    *this != chain;
}

String& String::append(const String& str) {
    const size_t newSize = this->length + str.length;

    char newStr[newSize];
    strcpy(newStr, this->str);
    strcat(newStr, str.str);

    return *this = newStr;
}

String& String::append(const char* chain) {
    return this->append(String(chain));
}

String& String::append(const char car) {
    return this->append(String(car));
}

String String::convertIntToString(const int num) const {
    size_t size = countNumberOfDigit(num);
    size++;
    if(num < 0) {
        size++;
    }
    char chain[size];
    snprintf(chain, size, "%d", num);
    return String(chain);
}

String String::convertFloatToString(const float num) const {
    int nbLeft = (int)num;
    String left = convertIntToString(nbLeft);

    const size_t rightSize = countNumberOfDigitDot(num);
    int nbRight = abs((int)((num - nbLeft) * pow(10, rightSize)));
    String right = convertIntToString(nbRight);

    return left.append(".").append(right);
}

const size_t String::countNumberOfDigit(int num) const {
    size_t cpt = 0;
    num = abs(num);
    do	{
        cpt++;
    }
    while((num /= 10) > 0);
    return cpt;
}

const size_t String::countNumberOfDigitDot(float num) const {
    size_t cpt = 0;
    while(num != (int)num) {
        cpt++;
        num *= 10;
    }
    return cpt;
}

