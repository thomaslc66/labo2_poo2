/*
 -----------------------------------------------------------------------------------
 Laboratoire : Laboratoire 14
 Fichier     : cstring.h
 Auteur(s)   : Thomas Lechaire et Michael Brouchoud
 Date        : 09.04.2017

 But         : Construction d'une class String

 Compilateur : Cygwin 2.4.1-1
 gcc 5.3.0
 g++ 5.3.0
 make 4.1
 gbd 7.10.1-1

 Remarque(s) : -

 -----------------------------------------------------------------------------------
 */

#ifndef CSTRING_H
#define CSTRING_H

#include <iostream>

using namespace std;
/*
 * @class String
 */
class String {

private:
    char* str;
    size_t length;

    /*
     * @fn friend ostream& operator << (ostream& stream, const String& str)
     *
     * @brief Surchage de l'operateur de flux
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param ostream& stream Le flux
     *
     * @param const String& str La String a afficher
     *
     * @return ostream& L'operateur de flux
     *
     */
    friend ostream& operator << (ostream& stream, const String& str);

    /*
    * @fn void iniString(const char* chaine)
    *
    * @brief Allouer dynamiquement la memoire et copier le contenu de la chaine de caractere dans la String
    *
    * @author Thomas Léchaire et Michael Brouchoud
    *
    * @param const char* chaine La chaine de caracteres
    */
    void iniString(const char* chaine);

public:

    /*
    * @fn char& operator[](int index)
    *
    * @brief Retourne le caractere se trouvant a la position index de la String
    *
    * @author Thomas Léchaire et Michael Brouchoud
    *
    * @throws out_of_range
    *
    * @param const size_t index L'index du caractere a retourner
    */
    char& operator[](const size_t index);

    /*
     * @fn String& operator = (const String& str)
     *
     * @brief Surchage de l'operateur d'affectation avec une String
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str La string a affecter
     *
     * @return String& La String nouvellement affecter
     */
    String& operator = (const String& str);

    /*
     * @fn String operator + (const String& str)
     *
     * @brief Surchage de l'operateur d'addition (concatenation de String) avec autre String
     *        Retourne la String par copie
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str Une autre String
     *
     * @return String& Une copie de l'addition de la String
     */
    String operator + (const String& str) const;


    /*
     * @fn String& operator += (const String& str)
     *
     * @brief Surchage de l'operateur d'addition (concatenation de String) avec autre String
     *        Retourne la String par reference
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str Une autre String
     *
     * @return String& Une reference de l'addition de la String
     */
    String& operator += (const String& str);

    /*
     * @fn String& operator = (const char* chain)
     *
     * @brief Surchage de l'operateur d'affectation avec une chaine de caracteres
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char* chain La chaine de caracteres a affecter
     *
     * @return String& La String nouvellement affecter
     */
    String& operator = (const char* chain);

    /*
     * @fn String operator + (const char* chain)
     *
     * @brief Surchage de l'operateur d'addition (concatenation de String) avec une chaine de caracteres.
     *        Retourne la String par copie
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char* chain La chaine de caracteres a additionner
     *
     * @return String& Une copie de l'addition de la String
     */
    String operator + (const char* chain) const;

    /*
     * @fn String& operator += (const char* chain)
     *
     * @brief Surchage de l'operateur d'addition (concatenation de String) avec une chaine de caracteres.
     *        Retourne la String par reference
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char* chain La chaine de caracteres a additionner
     *
     * @return String& Une reference de l'addition de la String
     */
    String& operator += (const char* chain);

    /*
     * @fn String operator + (const char car)
     *
     * @brief Surchage de l'operateur d'addition (concatenation de String) avec un caracteres.
     *        Retourne la String par copie
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char car Le caracteres a additionner
     *
     * @return String& Une copie de l'addition de la String
     */
    String operator + (const char car) const;

    /*
     * @fn String& operator += (const char car)
     *
     * @brief Surchage de l'operateur d'addition (concatenation de String) avec un caracteres.
     *        Retourne la String par reference
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char car Le caracteres a additionner
     *
     * @return String& Une reference de l'addition de la String
     */
    String& operator += (const char car);

    /*
     * @fn bool operator == (const String& str)
     *
     * @brief Surchage de l'operateur de comparaison
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str La String qu'il faut comparer
     *
     * @return true Le contenu de la String est égal à celui de str
     * @return false Le contenu de la String n'est pas égal à celui de str
     */
    bool operator == (const String& str) const;

    /*
     * @fn bool operator != (const String& str)
     *
     * @brief Surchage de l'operateur de comparaison
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str La String qu'il faut comparer
     *
     * @return false Le contenu de la String est égal à celui de str
     * @return true Le contenu de la String n'est pas égal à celui de str
     */
    bool operator != (const String& str) const;

    /*
     * @fn String()
     *
     * @brief Constructeur vide
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     */
    String();
    
    
    /*
     * @fn String(const char* chaine)
     *
     * @brief Constructeur à partir d'une chaîne de caractère
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @param const char* chaine La chaine de caractere
     * 
     */
    String(const char* chain);


    /*
     * @fn String(const String chaine)
     *
     * @brief Constructeur à partir d'une instance de la classe String
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @param const String chaine Une String
     * 
     */
    String(const String& chain);

    /*
     * @fn String(const char car)
     *
     * @brief Constructeur à partir d'un caractere
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @param const char car Un caractere
     * 
     */
    String(const char car);

    /*
     * @fn String(const int num)
     *
     * @brief Constructeur à partir d'un entier
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @param const int num Un entier
     * 
     */
    String(const int num);

    /*
     * @fn String(const float flt)
     *
     * @brief Constructeur à partir d'un reel
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @param const float flt Un reel
     * 
     */
    String(const float flt);

    /*
     * @fn String(const bool bln)
     *
     * @brief Constructeur à partir d'un booleen
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @param const bool bln Un booleen
     * 
     */
    String(const bool bln);

    /*
     * Destructeur
     */
    virtual ~String();

    /*
     * @fn int getSize() const
     *
     * @brief Méthode qui calcule la longeur de la chaine
     *
     * @author Thomas Léchaire et Michael Brouchoud
     * 
     * @return const size_t La longueur de la chaine
     * 
     */
    const size_t getSize() const;

    /*
     * @fn char& at(int index) const
     *
     * @brief Retourne le caractere se trouvant a la position i de la String
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @throw out_of_range
     *
     * @return const char Le caractere se trouvant a la position i de la String
     *
     */
    char& at(const size_t index) const;

    /*
     * @fn const char* print() const
     *
     * @brief Retourne un pointeur sur une chaine de caractere correspondant a la String
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @return const char* Un pointeur sur une chaine de caractere correspondant a la String
     *
     */
    const char* print() const;

    /*
     * @fn String substring(const size_t start, const size_t end) const
     *
     * @brief Retourne une sous-chaine de la String par copie entre les positions
     *        start (compris) et end (non-compris)
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @throws out_of_range
     *
     * @throws invalid_argument
     *
     * @return String Une copie de la sous-chaine
     *
     */
    String substring(const size_t start, const size_t end) const;

    /*
     * @fn void setString(const String& str)
     *
     * @brief Modifie la String avec la String passe en parametre
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str La String dont il faut prendre la valeur
     *
     */
    void setString(const String& str);

    /*
     * @fn void setString(const char* chain)
     *
     * @brief Modifie la String avec la chaine de caractere passee en parametre
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char* chain La chaine de caractere dont il faut prendre la valeur
     *
     */
    void setString(const char* chain);

    /*
     * @fn bool equals(const String& str) const
     *
     * @brief Permet de comparer deux String pour verfier que ce soit les memes
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str La String qu'il faut comparer
     *
     * @return true Le contenu de la String est égal à celui de str
     * @return false Le contenu de la String n'est pas égal à celui de str
     */
    bool equals(const String& str) const;

    /*
     * @fn bool equals(const String& str) const
     *
     * @brief Permet de comparer deux String pour verfier que ce soit les memes
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const String& str La String qu'il faut comparer
     *
     * @return true Le contenu de la String n'est pas égal à celui de str
     * @return false Le contenu de la String est égal à celui de str
     */
    bool notEquals(const String& str) const;

     /*
      * @fn bool equals(const char* chain) const
      *
      * @brief Permet de comparer une String à une chaine de caractere pour verfier que ce soit les memes
      *
      * @author Thomas Léchaire et Michael Brouchoud
      *
      * @param const char* chain a chaine de caractere qu'il faut comparer
      *
     * @return true Le contenu de la String n'est pas égal à celui de chain
     * @return false Le contenu de la String est égal à celui de chain
      */
    bool equals(const char* chain) const;

    /*
     * @fn bool notEquals(const char* chain) const
     *
     * @brief Permet de comparer une String à une chaine de caractere pour verfier que ce soit les memes
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char* chain a chaine de caractere qu'il faut comparer
     *
     * @return false Le contenu de la String n'est pas égal à celui de chain
     * @return true Le contenu de la String est égal à celui de chain
     */
    bool notEquals(const char* chain) const;

    /*
     * @fn String& appendStr(const String& str)
     *
     * @brief Concatene une String a une autre
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @throws out_of_range
     *
     * @param const String& str La String a concatener
     *
     * @return String& Une reference sur la String modifiée
     *
     */
    String& append(const String& str);

    /*
     * @fn String& appendCharPtr(const char* chain)
     *
     * @brief Concatene une String a une chaine de caractere
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char* chain Une chaine de caractere
     *
     * @return String& Une reference sur la String modifiée
     *
     */
    String& append(const char* chain);

    /*
     * @fn String& appendChar(const char car)
     *
     * @brief Concatene une String a un caractrere
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const char car Un caractrere
     *
     * @return String& Une reference sur la String modifiée
     *
     */
    String& append(const char car);

    /*
     * @fn String convertIntToString(const int num)
     *
     * @brief Permet de convertir un nombre entier en String
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const int num Le nombre a convertir
     *
     * @return String Le nombre converti en String
     *
     */
    String convertIntToString(const int num) const;

    /*
     * @fn String convertFloatToString(const float num)
     *
     * @brief Permet de convertir un reel en String
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param const int num Le reel a convertir
     *
     * @return String Le reel converti en String
     *
     */
    String convertFloatToString(const float num) const;

    /*
     * @fn const size_t  countNumberOfDigit(int num)
     *
     * @brief Compte le nombre de chiffre qu'il y a
     *        dans le nombre passé en paramètre
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param int num Le nombre dont il faut compter le nombre de chiffre
     *
     * @return const size_t Le nombre de chiffre dans le nombre
     *
     */
    const size_t  countNumberOfDigit(int num) const;

    /*
     * @fn const size_t countNumberOfDigitDot(float num)
     *
     * @brief Compte le nombre de chiffre apres la virgule qu'il y a
     *        dans le nombre passé en paramètre
     *
     * @author Thomas Léchaire et Michael Brouchoud
     *
     * @param int num Le nombre dont il faut compter le nombre de chiffre apres la virgule
     *
     * @return const size_t Le nombre de chiffre apres la virgule dans le nombre
     *
     */
    const size_t countNumberOfDigitDot(float num) const;

};



#endif /* CSTRING_H */

