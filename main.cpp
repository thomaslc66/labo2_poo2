/*
 -----------------------------------------------------------------------------------
 Laboratoire : Laboratoire 14
 Fichier     : cstring.h
 Auteur(s)   : Thomas Lechaire et Michael Brouchoud
 Date        : 09.04.2017

 But         : Tester la class String

 Compilateur : Cygwin 2.4.1-1
 gcc 5.3.0
 g++ 5.3.0
 make 4.1
 gbd 7.10.1-1

 Remarque(s) : -

 -----------------------------------------------------------------------------------
 */


#include "cstring.h"

using namespace std;

int main(int argc, char** argv) {

    //Creation String vide
    String stringEmpty;

    //Creation String char
    String stringChar('a');

	//Creation String char*
    String stringHello("hello");
    
    String stringSalut("salut");

    //Pour retablir la string stringSalut pendant les tests
    String stringSalutbak = stringSalut;
    
    //Creation String a partir d autre String
    String stringString(stringHello);

    //Creation String Boolean
    String stringBln(true);

    //Creation String nombre positif
    String posNumber(1234);

    //Creation String Negatif
    String negNumber(-128456);

    //Creation String reel positif
    String posReel(5873.445875f);

    //Creation String reel negatif
    String negReel(-5873.445875f);

    //Affichage
    cout << "------------------------------- Test de creation d'instances --------------------------------" << endl;
    cout << "---------------------------- Affichage des variables de départ ------------------------------" << endl;
    cout << "chaine vide (stringEmpty) : " << stringEmpty << endl;
    cout << "String Char (stringChar) : " << stringChar << endl;
    cout << "String Salut (stringSalut) : " << stringSalut << endl;
    cout << "String Hello (stringHello) : " << stringHello << endl;
    cout << "Construction d'une String à partir de stringHello (stringString) : " << stringString << endl;
    cout << "String Booleen (stringBln) : " << stringBln << endl;
    cout << "nombre positif (posNumber) : " << posNumber << endl;
    cout << "nombre negatif (negNumber) : " << negNumber << endl;
    cout << "float positif (posReel) : " << posReel << endl;
    cout << "float negatif (negReel) : " << negReel << endl;
    cout << endl;

    cout << "----------------------------------- Tests des methodes -------------------------------------" << endl;
    cout << "----------------------------- Determination de la longueur ---------------------------------" << endl;
    cout << stringHello  << " possède " << stringHello.getSize() << " lettres" << endl;
    cout << endl;

    cout << "---------------------- Représentation sous la forme de const char* -------------------------" << endl;
    cout << "Utilisation de String.print() pour afficher stringSalut sous forme de const char* : " << stringSalut.print() << endl;
    cout << endl;

    cout << "------------ Récupération du i-eme caractere pour d'eventuelle modifications ---------------" << endl;
    cout << "Modification de la String stringSalut avec l'index l'operateur[]" << endl;
    cout << "en copiant lettre par lettre stringHello dans stringSalut : " << endl;
    cout << "String Salut avant : " << stringSalut << endl;
    for(size_t i = 0; i < stringSalut.getSize(); i++ ){
        stringSalut[i] = stringHello[i];
    }
    cout << "String stringSalut après : " << stringSalut << endl;


    stringSalut = stringSalutbak; //Retablissement de l'état normal de string salut pour la suite des tests
    cout << "Retablissement de l'etat initial de stringSalut : " << stringSalut << endl;
    cout << endl;

    cout << "------------------------------------- Comparaisons ----------------------------------------" << endl;
    cout << "Est ce que " << stringSalut << " == " << stringHello << " : " << String(stringSalut == stringHello) << endl;
    cout << "Est ce que " << stringSalut << " != " << stringHello << " : " << String(stringSalut != stringHello) << endl;
    cout << endl;

    cout << "-------------- Operateur d'affectation avec une chaine de caracteres ou une String --------" << endl;
    stringSalut = "chameau";
    cout << "A partir d'une chaine de caractere valant \"chameau\" : " << stringSalut << endl;

    stringSalut = String("Moi je vais bien");
    cout << "A partir d'une String valant \"Moi je vais bien\" : " << stringSalut << endl;


    stringSalut = stringSalutbak; //Retablissement de l'état normal de string salut pour la suite des tests
    cout << "Retablissement de l'etat initial de stringSalut : " << stringSalut << endl;
    cout << endl;

    cout << "-------------------------- Concatenation de chaine de caracteres --------------------------" << endl;
    cout << "Concatenation de stringSalut avec chaine de caracteres valant \" ca va\" (operateur +) : " << stringSalut + " ca va" << endl;
    cout << "Valeur de stringSalut apres concatenation : " << stringSalut << endl;
    cout << endl;

    cout << "Concatenation de stringSalut avec chaine de caracteres valant \" ca va\" (operateur +=) : " << (stringSalut += " ca va") << endl;
    cout << "Valeur de stringSalut apres concatenation : " << stringSalut << endl;
    cout << endl;

    stringSalut = stringSalutbak; //Retablissement de l'état normal de string salut pour la suite des tests
    cout << "Retablissement de l'etat initial de stringSalut : " << stringSalut << endl;
    cout << endl;

    cout << "Concatenation de stringSalut avec une String valant \" ca va\" (operateur +) : " << stringSalut + String(" ca va") << endl;
    cout << "Valeur de stringSalut apres concatenation : " << stringSalut << endl;
    cout << endl;

    cout << "Concatenation de stringSalut avec une String \" ca va\" (operateur +=) : " << (stringSalut += String(" ca va")) << endl;
    cout << "Valeur de stringSalut apres concatenation : " << stringSalut << endl;
    cout << endl;

    stringSalut = stringSalutbak; //Retablissement de l'état normal de string salut pour la suite des tests
    cout << "Retablissement de l'etat initial de stringSalut : " << stringSalut << endl;
    cout << endl;

    cout << "Concatenation de stringSalut avec un caractere valant \"o\" (operateur +) : " << stringSalut + 'o' << endl;
    cout << "Valeur de stringSalut apres concatenation : " << stringSalut << endl;
    cout << endl;

    cout << "Concatenation de stringSalut avec un caractere \"o\" (operateur +=) : " << (stringSalut += 'o') << endl;
    cout << "Valeur de stringSalut apres concatenation : " << stringSalut << endl;
    cout << endl;

    stringSalut = stringSalutbak; //Retablissement de l'état normal de string salut pour la suite des tests
    cout << "Retablissement de l'etat initial de stringSalut : " << stringSalut << endl;
    cout << endl;

    cout << "------------------------------ Exctraction d'une sous-chaine ------------------------------" << endl;
    cout << "exctraction entre 2 et 7 de la chaine negReel (-5873.445875f) : " << negReel.substring(2, 7) << endl;
    cout << endl;

    cout << "--------------------------------- Tests des exceptions ------------------------------------" << endl;

    try {
        cout << "Acces à la position 25 de la chaine negReel : " << endl;
        negReel[25];
    }
    catch (out_of_range e) {
        cout << e.what() << endl;
    }
    cout << endl;

    try {
        cout << "Soustraction d'une sous chaine entre 25 et 4 de la chaine negReel : " << endl;
        negReel.substring(25, 4);
    }
    catch (invalid_argument e) {
        cout << e.what() << endl;
    }
    cout << endl;

    try {
        cout << "Soustraction d'une sous chaine entre 34 et 35 de la chaine negReel : " << endl;
        negReel.substring(34, 35);
    }
    catch (out_of_range e) {
        cout << e.what() << endl;
    }
    cout << endl;

    try {
        cout << "Modification du caractere à la position 50 de la chaine stringHello avec la lettre \"p\" : " << endl;
        stringHello[50] = 'p';
    }
    catch (out_of_range e) {
        cout << e.what() << endl;
    }

    cout << endl;

    cout << "------------------------------ Fin de l'ensemble des tests --------------------------------" << endl;

    return EXIT_SUCCESS;
}

